package pt.rumos.iot.mqtt_client.mysql;
import java.util.List;
import java.util.Optional;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MysqlControler {
    
    @Autowired
    private MysqlService service;

    @GetMapping("/history")
    public String getAll(Model m) {
        List<MysqlSensor> data = service.getAll();
        return new Gson().toJson(data);

    }

}
