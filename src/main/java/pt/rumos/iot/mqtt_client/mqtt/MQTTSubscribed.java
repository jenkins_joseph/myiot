package pt.rumos.iot.mqtt_client.mqtt;

import org.springframework.context.ApplicationListener;
import org.springframework.integration.mqtt.event.MqttSubscribedEvent;
import org.springframework.stereotype.Component;


@Component
public class MQTTSubscribed implements ApplicationListener<MqttSubscribedEvent> {

    @Override
    public void onApplicationEvent(MqttSubscribedEvent event) {
        System.out.println("Subscribed Success: " + event.getMessage());
    }

}